from django.http import JsonResponse

from . import models


def token(request):
    client_ip = request.META['REMOTE_ADDR']
    encoded_jwt = None
    if 'jwt' in request.GET:
        encoded_jwt = request.GET['jwt']
    print('In Request for', client_ip)

    obj = models.Job.objects.create_job(encoded_jwt=encoded_jwt)
    if obj:
        data = {
            'project_id': obj.project_id,
            'job_id': obj.job_id,
            'token': obj.token,
            'errors': '',
        }
    else:
        data = {
            'errors': 'Object was not able to be created'
        }
    return JsonResponse(data)
