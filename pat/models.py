import json
import sys
import threading
import time
from datetime import timedelta
from hashlib import sha256

from django.conf import settings
from django.db import models
import requests
import jwt
from django.utils import timezone

ORIGIN = settings.GITLAB_ORIGIN
API_VERSION = 'v4'
HEADERS = {
    'PRIVATE-TOKEN': settings.GITLAB_TOKEN
}

PERMISSION_KEY_OPTIONS = (
    ('namespace_id', ''),  # '7058175'
    ('namespace_path', ''),  # 'gitlab-gold/tpoffenbarger'
    ('project_id', ''),  # '18717904'
    ('project_path', ''),  # 'gitlab-gold/tpoffenbarger/ci-intro'
    ('user_id', ''),  # '428430'
    ('user_login', ''),  # 'poffey21'
    ('user_email', ''),  # 'tpoffenbarger@gitlab.com'
    ('pipeline_id', ''),  # '154513706'
    ('job_id', ''),  # '588118412'
    ('ref', ''),  # 'master'
    ('ref_type', ''),  # 'branch'
    ('ref_protected', ''),  # 'true'
    # ('jti', ''),  # '05d58c27-b61c-49ac-9a3d-ba61ac537bf2'
    ('iss', ''),  # gitlab.com
    ('iat', ''),  # 1591722089
    ('nbf', ''),  # 1591722084
    ('exp', ''),  # 1591725689
    ('sub', ''),  # 'job_588118412'
)

PERMISSION_OPERATOR_OPTIONS = (
    ('contains', 'Contains'),
    ('startswith', 'Starts With'),
    ('endswith', 'Ends With'),
    ('not in', 'Does Not Contain'),
    # ('', ''),
)


class Request(models.Model):
    """
    Every request will be recorded
    """

    role_search = models.CharField(blank=True, max_length=128)
    encoded_jwt = models.TextField(blank=True)
    decoded_jwt = models.TextField(blank=True)
    verified = models.NullBooleanField(blank=True, null=True)
    received_at = models.DateTimeField(auto_now_add=True)
    events = models.TextField(blank=True)

    def find_role(self):
        self.events = {
            'decode': 'Failure: Was not evaluated',
            'role': 'Failure: Was not evaluated',
            'permission_evaluation': 'Failure: Was not evaluated',
            'secret': 'Failure Was not evaluated',
        }
        # Decode and Save Decoded JWT
        step = 'decode'
        try:
            decoded_jwt = jwt.decode(self.encoded_jwt, verify=False)
            self.decoded_jwt = json.dumps(decoded_jwt)
            self.events[step] = 'Success: Decoded and saved the JWT'
            self.save()
        except jwt.exceptions.DecodeError as e:
            self.events[step] = 'Failure: ' + str(e)
            self.save()
            return

        # Verify Time and Source
        try:
            # https://gitlab.com/.well-known/openid-configuration
            # Check the Signature - https://auth0.com/docs/tokens/guides/validate-jwts#check-the-signature
            # Check Standard Claims - https://auth0.com/docs/tokens/guides/validate-jwts#check-standard-claims
            assert decoded_jwt['iss'] in settings.GITLAB_ORIGIN
        except AssertionError as e:
            self.decoded_jwt = ''
            self.events[step] = 'Failure: ' + str(e)
            self.save()



class JobQuerySet(models.QuerySet):
    """
    Creates jobs
    """

    def create_job(self, job_token=None, encoded_jwt=None):
        """
        This method finds the ci job and generates a token
        """

        if encoded_jwt:
            request = Request.objects.create(
                encoded_jwt=encoded_jwt
            )
            request.find_role()
            decoded_jwt = json.loads(request.decoded_jwt)
            project_id = decoded_jwt['project_id']
            job_id = decoded_jwt['job_id']
            username = decoded_jwt['user_login']
        else:
            return

        # Track down user
        get_user_endpoint = f'{ORIGIN}/api/{API_VERSION}/users?username={username}'
        try:
            user_id = requests.get(
                get_user_endpoint,
                headers=HEADERS,
            ).json()[0]['id']
        except (json.decoder.JSONDecodeError, IndexError, KeyError):
            print('Unable to find user')
            return

        # Create an Impersonation Token of User Initiating Job
        create_endpoint = f'{ORIGIN}/api/{API_VERSION}/users/{user_id}/impersonation_tokens'
        expires_at = timezone.now() + timedelta(days=2)
        create_data = {
            'user_id': user_id,
            'name': f'Auto-Generated for Project {project_id} Job {job_id}',
            'expires_at': expires_at.strftime('%Y-%m-%d'),  # '2020-01-25',
            'scopes': ['api']
        }

        try:
            r = requests.post(
                create_endpoint,
                headers=HEADERS,
                json=create_data,
            ).json()
            token = r['token']
            token_id = r['id']
        except (json.decoder.JSONDecodeError, KeyError) as e:
            return

        # Create an Impersonation Token Object but don't save the actual token
        token_sha = sha256(token.encode('UTF-8')).hexdigest()

        obj = self.create(
            project_id=project_id,
            job_id=job_id,
            user_id=user_id,
            token_id=token_id,
            token_sha=token_sha,
        )

        obj.token = token  # Set the token so that the view can return it

        # Run a background task to delete the token once the job is completed.
        t = threading.Thread(target=Job.revoke_token, args=(obj,))
        t.setDaemon(True)
        t.start()
        return obj


class Job(models.Model):
    """
    GitLab Job that has requested a Private Access Token
    """

    project_id = models.CharField(max_length=256)
    job_id = models.IntegerField()
    user_id = models.IntegerField()
    token_id = models.IntegerField()
    token_sha = models.CharField(max_length=256, blank=True)
    token_exposed = models.NullBooleanField()

    objects = JobQuerySet.as_manager()

    def revoke_token(self):
        """
        This ought to be run within a thread and will delete the Impersonation
        Token once it's created.

        Example:
            t = threading.Thread(target=Job.revoke_token, args=(obj,))
            t.setDaemon(True)
            t.start()
        """
        job_endpoint = f'{ORIGIN}/api/{API_VERSION}/projects/{self.project_id}/jobs/{self.job_id}'
        time.sleep(2)
        try:
            sys.stdout.write('Waiting for job to complete')
            while requests.get(job_endpoint, headers=HEADERS).json().get('status', '') in ['running']:
                sys.stdout.write('.')
                sys.stdout.flush()
                time.sleep(5)
            sys.stdout.write('\n')
        except json.decoder.JSONDecodeError:
            pass
        sys.stdout.write('')
        # TODO: Look for text in logs, set token_exposed and delete token_sha
        endpoint = f'{ORIGIN}/api/{API_VERSION}/users/{self.user_id}/impersonation_tokens/{self.token_id}'
        requests.delete(endpoint, headers=HEADERS)
        self.token_sha = ''
        self.save()
        sys.stdout.write('Token has been removed\n')
        sys.stdout.flush()
